﻿public class ShieldArmor : Armor
{
    private void Awake()
    {
        id = 4;
        itemName = "ShieldArmor";
        defensePower = 3;
        speedPenalty = 1;
    }
}
