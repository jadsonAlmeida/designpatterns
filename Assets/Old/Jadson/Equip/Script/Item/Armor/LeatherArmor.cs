﻿public class LeatherArmor : Armor
{
    private void Awake()
    {
        id = 1;
        itemName = "LeatherArmor";
        defensePower = 2;
        speedPenalty = 1;
    }
}
