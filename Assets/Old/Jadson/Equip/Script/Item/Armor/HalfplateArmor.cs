﻿public class HalfplateArmor : Armor
{
    private void Awake()
    {
        id = 2;
        itemName = "HalfplateArmor";
        defensePower = 5;
        speedPenalty = 3;
    }
}
