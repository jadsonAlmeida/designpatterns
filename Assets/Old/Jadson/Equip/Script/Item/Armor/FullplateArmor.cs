﻿public class FullplateArmor : Armor
{
    private void Awake()
    {
        id = 3;
        itemName = "FullplateArmor";
        defensePower = 8;
        speedPenalty = 8;
    }
}
