﻿public class Armor : ItemBase
{
    protected int defensePower;
    protected int speedPenalty;
    // add type (shield, shoulder, helmet, legs etc)

    public int DefensePower
    {
        get { return defensePower; }
        set
        {
            defensePower = value;
        }
    }

    public int SpeedPenalty
    {
        get { return speedPenalty; }
        set
        {
            speedPenalty = value;
        }
    }
}
