﻿public abstract class Weapon : ItemBase
{
    protected int attackPower;
    protected float attackSpeed;
    protected float attackRange;

    public int AttackPower
    {
        get { return attackPower; }
        set
        {
            attackPower = value;
        }
    }

    public float AttackSpeed
    {
        get { return attackSpeed; }
        set
        {
            attackSpeed = value;
        }
    }

    public float AttackRange
    {
        get { return attackRange; }
        set
        {
            attackRange = value;
        }
    }
}
