﻿public class Longsword : Weapon
{
    private void Awake()
    {
        id = 10;
        itemName = "Longsword";
        attackPower = 10;
        attackSpeed = 10;
        attackRange = 1f;
    }
}
