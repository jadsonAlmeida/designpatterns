﻿public class Greatsword : Weapon
{
    private void Awake()
    {
        id = 11;
        itemName = "Greatsword";
        attackPower = 20;
        attackSpeed = 15;
        attackRange = 1.2f;
    }
}
