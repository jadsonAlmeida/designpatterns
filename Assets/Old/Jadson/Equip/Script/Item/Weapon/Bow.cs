﻿public class Bow : Weapon
{
    private void Awake()
    {
        id = 12;
        itemName = "Bow";
        attackPower = 10;
        attackSpeed = 15;
        attackRange = 10f;
    }
}
