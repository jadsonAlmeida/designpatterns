﻿using UnityEngine;
using UnityEngine.UI;

public class HelperShield : MonoBehaviour
{
    public HelperWeapon helperWeapon;
    public Toggle toggleNoShield;
    public Jadson.State.Creature playerCreature;
    public GameObject playerBaseForItem;
    public Jadson.State.Creature enemyCreature;
    public GameObject enemyBaseForItem;
    public GameObject shieldWood;
    private GameObject shieldEquipped;
    private Jadson.State.Creature targetCreature;
    private GameObject targetBaseForItem;

    private void Awake()
    {
        targetCreature = playerCreature;
        targetBaseForItem = playerBaseForItem;
    }

    public void SetShield(int id)
    {
        GameObject shield = null;
        if (id == 1)
        {
            shield = shieldWood;
        }
        SetShield(shield);
    }

    private void SetShield(GameObject shield)
    {
        CleanShield();

        if (shield == null) return;

        Weapon wscript = targetCreature.weapon;
        if (wscript != null && (wscript.GetType() == typeof(Greatsword) || wscript.GetType() == typeof(Bow)))
        {
            helperWeapon.SetNoWeapon();
        }

        shieldEquipped = Instantiate(shield);
        shieldEquipped.transform.SetParent(targetBaseForItem.transform);
        shieldEquipped.transform.localPosition = Vector3.zero;
        shieldEquipped.transform.localScale = targetBaseForItem.transform.localScale;

        targetCreature.shield = shieldEquipped.GetComponent<ShieldArmor>();

    }

    private void CleanShield()
    {
        if (shieldEquipped == null) return;
        DestroyImmediate(shieldEquipped);
        targetCreature.shield = null;
    }

    public void SetNoShield()
    {
        Debug.Log("SetNoShield");
        toggleNoShield.isOn = true;
    }

    public void ChangeTarget(int i)
    {
        if (i == 1)
        {
            targetCreature = playerCreature;
            targetBaseForItem = playerBaseForItem;
        }
        if (i == 2)
        {
            targetCreature = enemyCreature;
            targetBaseForItem = enemyBaseForItem;
        }
    }
}
