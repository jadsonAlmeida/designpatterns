﻿using UnityEngine;

public class HelperArmor : MonoBehaviour
{
    public Jadson.State.Creature playerCreature;
    public GameObject playerBaseForItem;
    public Jadson.State.Creature enemyCreature;
    public GameObject enemyBaseForItem;
    public GameObject leatherArmor;
    public GameObject halfplateArmor;
    public GameObject fullplateArmor;
    private GameObject armorEquipped;
    private Jadson.State.Creature targetCreature;
    private GameObject targetBaseForItem;

    private void Awake()
    {
        targetCreature = playerCreature;
        targetBaseForItem = playerBaseForItem;
    }

    public void SetArmor(int id)
    {
        GameObject armor = null;
        if (id == 1)
        {
            armor = leatherArmor;
        }
        if (id == 2)
        {
            armor = halfplateArmor;
        }
        if (id == 3)
        {
            armor = fullplateArmor;
        }
        SetArmor(armor);
    }

    private void SetArmor(GameObject armor)
    {
        CleanArmor();

        if (armor == null) return;

        armorEquipped = Instantiate(armor);
        armorEquipped.transform.SetParent(targetBaseForItem.transform);
        armorEquipped.transform.localPosition = Vector3.zero;
        armorEquipped.transform.localScale = targetBaseForItem.transform.localScale;

        targetCreature.armor = armorEquipped.GetComponent<Armor>();
    }

    private void CleanArmor()
    {
        if (armorEquipped == null) return;
        DestroyImmediate(armorEquipped);
        targetCreature.armor = null;
    }

    public void ChangeTarget(int i)
    {
        if (i == 1)
        {
            targetCreature = playerCreature;
            targetBaseForItem = playerBaseForItem;
        }
        if (i == 2)
        {
            targetCreature = enemyCreature;
            targetBaseForItem = enemyBaseForItem;
        }
    }
}
