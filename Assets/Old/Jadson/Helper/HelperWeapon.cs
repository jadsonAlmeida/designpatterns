﻿using UnityEngine;
using UnityEngine.UI;

public class HelperWeapon : MonoBehaviour
{
    public HelperShield helperShield;
    public Toggle toggleNoWeaponOff;
    public Jadson.State.Creature playerCreature;
    public GameObject playerBaseForItem;
    public Jadson.State.Creature enemyCreature;
    public GameObject enemyBaseForItem;
    public GameObject longsword;
    public GameObject greatsword;
    public GameObject bow;
    private GameObject weaponEquipped;
    private Jadson.State.Creature targetCreature;
    private GameObject targetBaseForItem;

    private void Awake()
    {
        targetCreature = playerCreature;
        targetBaseForItem = playerBaseForItem;
    }

    public void SetWeapon(int id)
    {
        GameObject weapon = null;
        if (id == 1)
        {
            weapon = longsword;
        }
        if (id == 2)
        {
            weapon = greatsword;
        }
        if (id == 3)
        {
            weapon = bow;
        }
        SetWeapon(weapon);
    }

    private void SetWeapon(GameObject weapon)
    {
        CleanWeapon();

        if (weapon == null) return;

        Weapon wscript = weapon.GetComponent<Weapon>();
        if ((wscript.GetType() == typeof(Greatsword) || wscript.GetType() == typeof(Bow)) && targetCreature.shield != null)
        {
            helperShield.SetNoShield();
            // real situation code
            /*
                if (creature.shield.GetType() == typeof(ShieldArmor))
                {
                    DestroyImmediate(creature.shield.gameObject);
                }
            */
        }

        weaponEquipped = Instantiate(weapon);
        weaponEquipped.transform.SetParent(targetBaseForItem.transform);
        weaponEquipped.transform.localPosition = Vector3.zero;
        weaponEquipped.transform.localScale = targetBaseForItem.transform.localScale;

        targetCreature.weapon = weaponEquipped.GetComponent<Weapon>();
    }

    private void CleanWeapon()
    {
        if (weaponEquipped == null) return;
        DestroyImmediate(weaponEquipped);
        targetCreature.weapon = null;
    }

    public void SetNoWeapon()
    {
        Debug.Log("SetNoWeapon");
        toggleNoWeaponOff.isOn = true;
    }

    public void ChangeTarget(int i)
    {
        if (i == 1)
        {
            targetCreature = playerCreature;
            targetBaseForItem = playerBaseForItem;
        }
        if (i == 2)
        {
            targetCreature = enemyCreature;
            targetBaseForItem = enemyBaseForItem;
        }
    }
}
