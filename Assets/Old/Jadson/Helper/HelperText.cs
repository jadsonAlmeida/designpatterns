﻿using UnityEngine;
using UnityEngine.UI;

public class HelperText : MonoBehaviour
{
    public static HelperText instance;
    public Text debugText;

    private void Awake()
    {
        instance = this;
    }
}
