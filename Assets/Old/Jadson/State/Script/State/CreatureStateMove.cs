﻿using UnityEngine;

namespace Jadson.State
{
    public class CreatureStateMove : ICreatureState
    {
        private Vector3 direction;

        public CreatureStateMove(Vector3 direction)
        {
            this.direction = direction;
        }

        public void DoAction(Creature creature)
        {
            creature.transform.localPosition += direction * Time.deltaTime * creature.speedMoving;
            creature.State = new CreatureStateIdle();
        }
    }
}
