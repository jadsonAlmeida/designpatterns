﻿using UnityEngine;

namespace Jadson.State
{
    public class CreatureStateJump : ICreatureState
    {
        private float jumpPower;

        public CreatureStateJump(float jumpPower)
        {
            this.jumpPower = jumpPower;
        }

        public void DoAction(Creature creature)
        {
            Vector3 jump = Vector3.up * jumpPower;
            creature.GetComponent<Rigidbody>().AddForce(jump);
            creature.State = new CreatureStateIdle();
        }
    }
}