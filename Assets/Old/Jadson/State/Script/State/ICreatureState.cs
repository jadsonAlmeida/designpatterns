﻿namespace Jadson.State
{
    public interface ICreatureState
    {
        void DoAction(Creature creature);
    }
}
