﻿using UnityEngine;

namespace Jadson.State
{
    public class CreatureStateAttack : ICreatureState
    {
        public void DoAction(Creature creature)
        {
            // check delay to attack
            if (!creature.CanAttack) return;

            Weapon weapon = creature.Weapon;
            float attackRange = creature.attackRange;
            if (weapon != null)
            {
                attackRange += weapon.AttackRange;
            }

            // visual: draw line-range attack
            Debug.DrawRay(creature.transform.localPosition, creature.transform.forward * attackRange, Color.yellow, 0.5f);

            // test range attack
            RaycastHit hitInfo;
            if (Physics.Raycast(creature.transform.localPosition, creature.transform.forward, out hitInfo, attackRange))
            {
                Creature opponentCreatureScript = hitInfo.collider.gameObject.GetComponent<Creature>();  // Real Project: need be a "hittable" interface

                if (opponentCreatureScript == null) return; // check hittable

                // deal damage
                int attackPower = creature.attackPower;
                if (weapon != null)
                {
                    attackPower += weapon.AttackPower;
                }
                HelperText.instance.debugText.text += "\n<b>"+creature.name + "</b>: Take my attack with " + attackPower + " power!";
                opponentCreatureScript.State = new CreatureStateHit(attackPower);

                creature.CanAttack = false; // set delay
                creature.State = new CreatureStateIdle();
            }
        }


    }
}
