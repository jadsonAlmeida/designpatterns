﻿using UnityEngine;

namespace Jadson.State
{
    public class CreatureStateHit : ICreatureState
    {
        private int attackPower;

        public CreatureStateHit(int attackPower)
        {
            this.attackPower = attackPower;
        }

        public void DoAction(Creature creature)
        {
            int defensePower = creature.defensePower;
            Armor armor = creature.armor;
            ShieldArmor shield = creature.shield;
            if (armor != null)
            {
                defensePower += armor.DefensePower;
            }
            if (shield != null)
            {
                defensePower += shield.DefensePower;
            }

            int damage = attackPower - defensePower;
            if (damage < 1)
            {
                damage = 0;
                HelperText.instance.debugText.text += "\n<b>" + creature.name + "</b>: Hahaha! You can't hurt me!";
            }
            else
            {
                creature.hitPoints -= damage;

                if (creature.hitPoints < 1)
                {
                    creature.Die();
                    return;
                }

                HelperText.instance.debugText.text += "\n<b>" + creature.name + "</b>: Fuck! I take " + damage + " damage! I have just " + creature.hitPoints;
            }

            creature.State = new CreatureStateIdle();
        }
    }
}
