﻿using UnityEngine;

namespace Jadson.State
{
    public class Creature : MonoBehaviour
    {
        #region Attributes

        private ICreatureState state;
        // Movement
        private Vector3 directionMoving; // DELETE
        public float speedMoving;
        public float jumpPower;
        // Battle
        public int attackPower;
        public float attackDelay;
        public float attackRange;
        public int defensePower;
        public int hitPoints;
        private bool canAttack = true;
        // Equip
        public Weapon weapon;
        public Armor armor;
        public ShieldArmor shield;

        #endregion Attributes

        public void DoAction()
        {
            state.DoAction(this);
        }

        public void Die()
        {
            HelperText.instance.debugText.text += "\n<b>" + name + "</b>: aaaarrrggg... I'm gonna die...";
            DestroyImmediate(gameObject);
        }

        private void ResetCanAttack()
        {
            CanAttack = true;
        }
        
        #region Gets and Setters
        public ICreatureState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
                DoAction();
            }
        }

        public Weapon Weapon
        {
            get
            {
                return weapon;
            }
            set
            {
                weapon = value;
            }
        }

        public Armor Armor
        {
            get
            {
                return armor;
            }
            set
            {
                armor = value;
            }
        }

        public ShieldArmor Shield
        {
            get
            {
                return shield;
            }
            set
            {
                shield = value;
            }
        }

        public Vector3 DirectionMoving
        {
            get
            {
                return directionMoving;
            }
            set
            {
                directionMoving = value;
            }
        }

        public bool CanAttack
        {
            get
            {
                return canAttack;
            }
            set
            {
                canAttack = value;
                if (!canAttack) Invoke("ResetCanAttack", attackDelay);
            }
        }
        #endregion  Gets and Setters
    }
}