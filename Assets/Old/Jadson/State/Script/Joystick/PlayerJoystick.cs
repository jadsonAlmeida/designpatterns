﻿using UnityEngine;

namespace Jadson.State
{
    [RequireComponent(typeof(Creature))]
    public class PlayerJoystick : MonoBehaviour
    {
        public float speedRotation = 2;
        private Creature creatureScript;

        private void Start()
        {
            creatureScript = GetComponent<Creature>();
        }

        private void Update()
        {
            KeyboardController();
        }

        private void KeyboardController()
        {
            if (Input.GetKey(KeyCode.A))
            {
                creatureScript.transform.Rotate(Vector3.down * speedRotation);
            }
            if (Input.GetKey(KeyCode.D))
            {
                creatureScript.transform.Rotate(Vector3.up * speedRotation);
            }
            if (Input.GetKey(KeyCode.W))
            {
                creatureScript.State = new CreatureStateMove(creatureScript.transform.forward);
            }
            if (Input.GetKey(KeyCode.S))
            {
                creatureScript.State = new CreatureStateMove(-creatureScript.transform.forward);
            }
            if (Input.GetKey(KeyCode.L))
            {
                creatureScript.State = new CreatureStateAttack();
            }
            if (Input.GetKey(KeyCode.Space))
            {
                creatureScript.State = new CreatureStateJump(creatureScript.jumpPower);
            }
        }
    }
}