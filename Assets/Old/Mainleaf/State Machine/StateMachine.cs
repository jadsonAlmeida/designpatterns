﻿using UnityEngine;
using System.Collections;
using System;

public class StateMachine : MonoBehaviour
{
    #region Properties
    private Enum _state;

    public Enum State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            ConfigureCurrentState();
        }
    }
    #endregion

    #region Delegates
    public Action DoUpdate = DoNothing;
    public Action DoLateUpdate = DoNothing;
    public Action DoFixedUpdate = DoNothing;
    public Action<Collider> DoOnTriggerEnter = DoNothingCollider;
    public Action<Collider2D> DoOnTriggerEnter2D = DoNothingCollider2D;
    public Action<Collider> DoOnTriggerStay = DoNothingCollider;
    public Action<Collider> DoOnTriggerExit = DoNothingCollider;
    public Action<Collider2D> DoOnTriggerExit2D = DoNothingCollider2D;
    public Action<Collision> DoOnCollisionEnter = DoNothingCollision;
    public Action<Collision> DoOnCollisionStay = DoNothingCollision;
    public Action<Collision> DoOnCollisionExit = DoNothingCollision;
    public Action DoOnMouseEnter = DoNothing;
    public Action DoOnMouseUp = DoNothing;
    public Action DoOnMouseDown = DoNothing;
    public Action DoOnMouseOver = DoNothing;
    public Action DoOnMouseExit = DoNothing;
    public Action DoOnMouseDrag = DoNothing;
    public Action DoOnGUI = DoNothing;
    public Func<IEnumerator> ExitState = DoNothingCoroutine;
    #endregion

    #region Unity Events
    void Update()
    {
        DoUpdate();
    }

    void LateUpdate()
    {
        DoLateUpdate();
    }

    void OnMouseEnter()
    {
        DoOnMouseEnter();
    }

    void OnMouseUp()
    {
        DoOnMouseUp();
    }

    void OnMouseDown()
    {
        DoOnMouseDown();
    }

    void OnMouseExit()
    {
        DoOnMouseExit();
    }

    void OnMouseDrag()
    {
        DoOnMouseDrag();
    }

    void FixedUpdate()
    {
        DoFixedUpdate();
    }

    void OnTriggerEnter(Collider other)
    {
        DoOnTriggerEnter(other);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        DoOnTriggerEnter2D(other);
    }

    void OnTriggerStay(Collider other)
    {
        DoOnTriggerStay(other);
    }

    void OnTriggerExit(Collider other)
    {
        DoOnTriggerExit(other);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        DoOnTriggerExit2D(other);
    }

    void OnCollisionEnter(Collision other)
    {
        DoOnCollisionEnter(other);
    }

    void OnCollisionStay(Collision other)
    {
        DoOnCollisionStay(other);
    }

    void OnCollisionExit(Collision other)
    {
        DoOnCollisionExit(other);
    }

    void OnGUI()
    {
        DoOnGUI();
    }
    #endregion

    #region Method Stubs
    private static void DoNothing()
    {

    }
    private static void DoNothingCollider(Collider obj)
    {

    }
    private static void DoNothingCollider2D(Collider2D obj)
    {

    }
    private static void DoNothingCollision(Collision obj)
    {

    }
    private static IEnumerator DoNothingCoroutine()
    {
        yield break;
    }
    #endregion

    #region Private Methods
    void ConfigureCurrentState()
    {
        if (ExitState != null)
        {
            StartCoroutine(ExitState());
        }

        DoUpdate = ConfigureDelegate<Action>("Update", DoNothing);
        DoOnGUI = ConfigureDelegate<Action>("OnGUI", DoNothing);
        DoLateUpdate = ConfigureDelegate<Action>("LateUpdate", DoNothing);
        DoFixedUpdate = ConfigureDelegate<Action>("FixedUpdate", DoNothing);
        DoOnMouseUp = ConfigureDelegate<Action>("OnMouseUp", DoNothing);
        DoOnMouseDown = ConfigureDelegate<Action>("OnMouseDown", DoNothing);
        DoOnMouseEnter = ConfigureDelegate<Action>("OnMouseEnter", DoNothing);
        DoOnMouseExit = ConfigureDelegate<Action>("OnMouseExit", DoNothing);
        DoOnMouseDrag = ConfigureDelegate<Action>("OnMouseDrag", DoNothing);
        DoOnMouseOver = ConfigureDelegate<Action>("OnMouseOver", DoNothing);
        DoOnTriggerEnter = ConfigureDelegate<Action<Collider>>("OnTriggerEnter", DoNothingCollider);
        DoOnTriggerEnter2D = ConfigureDelegate<Action<Collider2D>>("OnTriggerEnter2D", DoNothingCollider2D);
        DoOnTriggerExit = ConfigureDelegate<Action<Collider>>("OnTriggerExit", DoNothingCollider);
        DoOnTriggerExit2D = ConfigureDelegate<Action<Collider2D>>("OnTriggerExit2D", DoNothingCollider2D);
        DoOnTriggerStay = ConfigureDelegate<Action<Collider>>("OnTriggerStay", DoNothingCollider);
        DoOnCollisionEnter = ConfigureDelegate<Action<Collision>>("OnCollisionEnter", DoNothingCollision);
        DoOnCollisionExit = ConfigureDelegate<Action<Collision>>("OnCollisionExit", DoNothingCollision);
        DoOnCollisionStay = ConfigureDelegate<Action<Collision>>("OnCollisionStay", DoNothingCollision);
        Func<IEnumerator> enterState = ConfigureDelegate<Func<IEnumerator>>("EnterState", DoNothingCoroutine);
        ExitState = ConfigureDelegate<Func<IEnumerator>>("ExitState", DoNothingCoroutine);
        //Optimisation, turn off GUI if we don't
        //have an OnGUI method
        //EnableGUI();

        StartCoroutine(enterState());
    }

    T ConfigureDelegate<T>(string methodRoot, T Default) where T : class
    {
        //Find a method called CURRENTSTATE_METHODROOT
        var method = GetType().GetMethod(_state.ToString() + "_" + methodRoot, System.Reflection.BindingFlags.Instance
            | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.InvokeMethod);

        if (method != null)
        {
            return Delegate.CreateDelegate(typeof(T), this, method) as T;
        }
        return Default;
    }
    #endregion
}