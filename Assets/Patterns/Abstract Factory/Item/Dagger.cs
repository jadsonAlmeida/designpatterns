/**
 * Dagger.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 23/01/17 (dd/mm/yy)
 * Revised on: 23/01/17 (dd/mm/yy)
 */
namespace AbstractFactory
{
    public class Dagger : Weapon
    {
        void Awake()
        {
            Damage = 4;
            Range = 1;
            AttackSpeed = 5;
        }
    }
}