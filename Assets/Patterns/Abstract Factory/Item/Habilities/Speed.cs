/**
* Speed.cs
* Created by: Jadson Almeida [jadson.sistemas@gmail.com]
* Created on: 23/01/17 (dd/mm/yy)
* Revised on: 23/01/17 (dd/mm/yy)
*/
namespace AbstractFactory
{
    public class Speed : Hability
    {
        public override void Active()
        {
            print("Speed hability actived.");
        }
    }
}