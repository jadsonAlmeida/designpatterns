/**
 * Hability.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 23/01/17 (dd/mm/yy)
 * Revised on: 24/01/17 (dd/mm/yy)
 */
using UnityEngine;

namespace AbstractFactory
{
    public abstract class Hability : MonoBehaviour
    {
        public abstract void Active();
    }
}