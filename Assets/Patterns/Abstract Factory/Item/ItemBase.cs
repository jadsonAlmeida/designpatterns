/**
 * ItemBase.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 23/01/17 (dd/mm/yy)
 * Revised on: 23/01/17 (dd/mm/yy)
 */
using UnityEngine;

namespace AbstractFactory
{
    public abstract class ItemBase : MonoBehaviour
    {
        public Hability hability;

        public void Equip()
        {
            print(name + " is Equipped.");
            hability.Active();
        }
    }
}