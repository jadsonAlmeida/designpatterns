/**
 * Hide.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 23/01/17 (dd/mm/yy)
 * Revised on: 23/01/17 (dd/mm/yy)
 */
namespace AbstractFactory
{
    public class Hide : Armor
    {
        void Awake()
        {
            Defense = 3;
        }
    }
}