/**
 * Armor.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 23/01/17 (dd/mm/yy)
 * Revised on: 23/01/17 (dd/mm/yy)
 */
namespace AbstractFactory
{
    public abstract class Armor : ItemBase
    {
        public int Defense { get; protected set; }
    }
}