/**
 * Weapon.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 23/01/17 (dd/mm/yy)
 * Revised on: 23/01/17 (dd/mm/yy)
 */
namespace AbstractFactory
{
    public class Weapon : ItemBase
    {
        public int Damage { get; protected set; }
        public int Range { get; protected set; }
        public int AttackSpeed { get; protected set; }
    }
}
