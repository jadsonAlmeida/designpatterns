/**
* Character.cs
* Created by: Jadson Almeida [jadson.sistemas@gmail.com]
* Created on: 24/01/18 (dd/mm/yy)
* Revised on: 24/01/18 (dd/mm/yy)
*/
using UnityEngine;

namespace AbstractFactory
{
    public class Character : MonoBehaviour
    {
        [SerializeField]
        float speed = 1;

        void Update()
        {
            InputHandler();
        }

        void InputHandler()
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += Vector3.forward * Time.deltaTime * speed;
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position += Vector3.right * Time.deltaTime * -speed;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position += Vector3.forward * Time.deltaTime * -speed;
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += Vector3.right * Time.deltaTime * speed;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("Collectable"))
            {
                DropItem();
                PickupItem(other.gameObject);
            }
        }

        void DropItem()
        {
            GameObject currentItem = FindChildWithTag("Collectable");
            if (currentItem == null)
                return;
            currentItem.transform.parent = null;
            Vector3 position = transform.position;
            position.z += transform.localScale.z + 1;
            currentItem.transform.position = position;
        }

        void PickupItem(GameObject item)
        {
            item.transform.SetParent(transform);
            Vector3 position = transform.position;
            position.x += transform.localScale.x / 2;
            item.transform.position = position;
            item.GetComponent<ItemBase>().Equip();
        }

        GameObject FindChildWithTag(string tag)
        {
            foreach (Transform tr in transform)
            {
                if (tr.tag == tag)
                {
                    return tr.gameObject;
                }
            }
            return null;
        }
    }
}