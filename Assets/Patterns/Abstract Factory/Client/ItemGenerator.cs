/**
* ItemGenerator.cs
* Created by: Jadson Almeida [jadson.sistemas@gmail.com]
* Created on: 24/01/17 (dd/mm/yy)
* Revised on: 24/01/17 (dd/mm/yy)
*/
using UnityEngine;

namespace AbstractFactory
{
    public class ItemGenerator : MonoBehaviour
    {
        void Awake()
        {
            GenerateItems();
        }

        /// <summary>
        /// Generate 5 items with random combinations
        /// </summary>
        void GenerateItems()
        {
            // create factorys
            ItemAbstractFactory itemBaseFactory = new ItemBaseFactory();
            ItemAbstractFactory habilityFactory = new ItemHabilityFactory();
            // create 5 items with random combinations
            for (int i = 0; i < 5; i++)
            {
                // create the item
                GameObject itemObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                itemObj.tag = "Collectable";
                itemObj.GetComponent<Collider>().isTrigger = true;
                // select a random item base type
                int itemBaseIndex = Random.Range(0, System.Enum.GetNames(typeof(ItemBaseType)).Length);
                // create a random item base
                itemBaseFactory.CreateItemBase(itemObj, (ItemBaseType)itemBaseIndex);
                // select a random hability type
                int itemHabilityIndex = Random.Range(0, System.Enum.GetNames(typeof(HabilityType)).Length);
                // create a random hability
                habilityFactory.CreateItemHability(itemObj, (HabilityType)itemHabilityIndex);
                // set a random position for item object
                itemObj.transform.position = new Vector3(Random.Range(-5, 5), 1, Random.Range(-5, 5));
                // set the object name to facilitate the test
                itemObj.name = ((ItemBaseType)itemBaseIndex).ToString() + "-" + ((HabilityType)itemHabilityIndex).ToString();
            }
        }
    }
}