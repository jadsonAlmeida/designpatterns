/**
* ItemHabilityFactory.cs
* Created by: Jadson Almeida [jadson.sistemas@gmail.com]
* Created on: 23/01/17 (dd/mm/yy)
* Revised on: 24/01/17 (dd/mm/yy)
*/
using UnityEngine;

namespace AbstractFactory
{
    public class ItemHabilityFactory : ItemAbstractFactory
    {
        public override ItemBase CreateItemBase(GameObject obj, ItemBaseType type)
        {
            return null;
        }

        public override Hability CreateItemHability(GameObject obj, HabilityType type)
        {
            switch (type)
            {
                case HabilityType.Speed:
                    return obj.GetComponent<ItemBase>().hability = obj.AddComponent<Speed>();
                case HabilityType.Heal:
                    return obj.GetComponent<ItemBase>().hability = obj.AddComponent<Heal>();
                case HabilityType.Flame:
                    return obj.GetComponent<ItemBase>().hability = obj.AddComponent<Flame>();
                default:
                    return null;
            }
        }
    }
}