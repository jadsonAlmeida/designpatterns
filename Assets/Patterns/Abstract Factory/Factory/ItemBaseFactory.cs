/**
* ItemBaseFactory.cs
* Created by: Jadson Almeida [jadson.sistemas@gmail.com]
* Created on: 23/01/17 (dd/mm/yy)
* Revised on: 24/01/17 (dd/mm/yy)
*/
using UnityEngine;

namespace AbstractFactory
{
    public class ItemBaseFactory : ItemAbstractFactory
    {
        public override ItemBase CreateItemBase(GameObject obj, ItemBaseType type)
        {
            switch (type)
            {
                case ItemBaseType.Hide:
                    return obj.AddComponent<Hide>();
                case ItemBaseType.Splice:
                    return obj.AddComponent<Splice>();
                case ItemBaseType.Dagger:
                    return obj.AddComponent<Dagger>();
                case ItemBaseType.Sunblade:
                    return obj.AddComponent<Sunblade>();
                case ItemBaseType.Bow:
                    return obj.AddComponent<Bow>();
                default:
                    return null;
            }
        }

        public override Hability CreateItemHability(GameObject obj, HabilityType type)
        {
            return null;
        }
    }
}