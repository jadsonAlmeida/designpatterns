/**
 * Projectile.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 01/08/17 (dd/mm/yy)
 */

using UnityEngine;
/// <summary>
/// Handles the projectiles attributes and behaviors
/// </summary>
public class Projectile : MonoBehaviour, IReusable
{
    /// <summary>
    /// Point to start the OverlapSphere to check collision with it
    /// </summary>
    [SerializeField]
    Transform collisionPoint;
    /// <summary>
    /// Direction and velocity of the automovement of this object
    /// </summary>
    [SerializeField]
    Vector3 autoMovement;

    /// <summary>
    /// If this projectile was fired
    /// </summary>
    bool isRunning;

    void Update()
    {
        if (isRunning)
            Move();
    }

    void FixedUpdate()
    {
        if (isRunning)
            CheckCollision();
    }

    /// <summary>
    /// Do this automatic movement based in <see cref="autoMovement"/>
    /// </summary>
    void Move()
    {
        transform.Translate(autoMovement * Time.deltaTime);
    }

    /// <summary>
    /// Checks if collide with something
    /// </summary>
    void CheckCollision()
    {
        if (Physics.OverlapSphere(collisionPoint.position, collisionPoint.localScale.x).Length > 0)
        {
            print(name + " Hit the wall");
            Store();
        }
    }

    /// <summary>
    /// Actives the <see cref="body"/>
    /// </summary>
    public void Reuse()
    {
        isRunning = true;
    }

    /// <summary>
    /// Disables the <see cref="body"/> and store this object in <see cref="ReusableManager"/>
    /// </summary>
    public void Store()
    {
        isRunning = false;
        ReusableManager.Instance.Store(gameObject);
    }
}
