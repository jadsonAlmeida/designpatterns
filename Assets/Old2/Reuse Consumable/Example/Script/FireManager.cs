/**
 * FireManager.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 01/08/17 (dd/mm/yy)
 */

using UnityEngine;
/// <summary>
/// Handles players input to fire reusable projectiles
/// </summary>
public class FireManager : MonoBehaviour
{
    /// <summary>
    /// Get player 'A' (fire arrow) and 'B' (fire bullet) inputs 
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            FireArrow();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            FireBullet();
        }
    }

    /// <summary>
    /// Fire a reusable Arrow
    /// </summary>
    void FireArrow()
    {
        GameObject arrow = ReusableManager.Instance.GetReusable("Arrow");
        FireProjectle(arrow);
    }

    /// <summary>
    /// Fire a reusable Bullet
    /// </summary>
    void FireBullet()
    {
        GameObject bullet = ReusableManager.Instance.GetReusable("Bullet");
        FireProjectle(bullet);
    }

    /// <summary>
    /// Updates the reusable projectile position and active it to fire
    /// </summary>
    /// <param name="projectle">The gameobject projectile to fire</param>
    void FireProjectle(GameObject projectle)
    {
        projectle.transform.position = transform.position;
        projectle.SetActive(true);
        IReusable reusableScript = projectle.GetComponent<IReusable>();
        reusableScript.Reuse();
    }
}
