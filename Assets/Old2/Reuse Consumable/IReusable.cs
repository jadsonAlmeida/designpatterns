/**
 * IReusable.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 01/08/17 (dd/mm/yy)
 */

/// <summary>
/// Represents whats the GameObject must do to be a reusable object, handled by <see cref="ReusableManager"/>
/// </summary>
public interface IReusable
{
    /// <summary>
    /// Disables the necessary behaviors and store this object in <see cref="ReusableManager"/>
    /// </summary>
    void Store();
    /// <summary>
    /// Enables the object behaviors
    /// </summary>
    void Reuse();
}