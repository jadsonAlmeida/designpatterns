/**
 * ReusableManager.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 01/08/17 (dd/mm/yy)
 */

using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// Its a singleton class that handles the reusable objects
/// </summary>
public sealed class ReusableManager : MonoBehaviour
{
    /// <summary>
    /// Access the singleton instance of this class
    /// </summary>
    public static ReusableManager Instance { get; private set; }

    /// <summary>
    /// Each <see cref="GameObject"/> with <see cref="IReusable"/> that be managed 
    /// </summary>
    [SerializeField]
    List<GameObject> reusableObjects = new List<GameObject>();
    /// <summary>
    /// The index order must the same of <see cref="reusableObjects"/>. Tells how many instances must be created in <see cref="poolList"/>
    /// for each index in <see cref="reusableObjects"/>
    /// </summary>
    [SerializeField]
    List<int> poolSizeByReusableObjectIndex = new List<int>();

    /// <summary>
    /// Generic pool that store all reusable objects using string with object.tag as key
    /// </summary>
    Dictionary<string, Queue<GameObject>> poolList = new Dictionary<string, Queue<GameObject>>();

    void Awake()
    {
        if (Instance != null)
            Debug.LogWarning("Already exist another static ReusableManager as singleton.");
        Instance = this;
        LoadReusableObjects();
    }

    /// <summary>
    /// Checks if object have <see cref="IReusable"/>. If true, instanciate it and store in <see cref="poolList"/>
    /// </summary>
    void LoadReusableObjects()
    {
        for (int i = 0; i < reusableObjects.Count; i++)
        {
            if (reusableObjects[i].GetComponentInChildren<IReusable>() == null)
            {
                Debug.Log("Can't reuse a object without IReusable interface.");
                continue;
            }
            for (int j = 0; j < poolSizeByReusableObjectIndex[i]; j++)
            {
                GameObject obj = Instantiate(reusableObjects[i], Vector3.zero, Quaternion.identity, transform);
                obj.GetComponentInChildren<IReusable>().Store();
            }
        }
    }

    /// <summary>
    /// Stores a reusable <see cref="GameObject"/> in <see cref="poolList"/>
    /// </summary>
    /// <param name="obj">Reusable GameObject</param>
    public void Store(GameObject obj)
    {
        obj.SetActive(false);
        string tag = obj.tag;
        // check if already exist a appropriate queue for this object
        if (!poolList.ContainsKey(tag))
        {
            poolList.Add(tag, new Queue<GameObject>());
        }
        // put object in appropriate queue
        poolList[tag].Enqueue(obj);
    }

    /// <summary>
    /// Returns a reusable object from <see cref="poolList"/> with tag if exist a queue
    /// with a tag or return null
    /// </summary>
    /// <param name="tag">tag of the reusable object</param>
    /// <returns>reusable object with a tag or null</returns>
    public GameObject GetReusable(string tag)
    {
        if (poolList.ContainsKey(tag))
        {
            return poolList[tag].Dequeue();
        }
        return null;
    }

    /// <summary>
    /// Deletes all reusable objects with a tag
    /// </summary>
    /// <param name="tag">tag of the reusable objects to delete</param>
    public void CleanReusableList(string tag)
    {
        poolList[tag].Clear();
        poolList.Remove(tag);
    }
}
