/**
 * DataBehavior.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 14/08/17 (dd/mm/yy)
 * Revised on: 25/08/17 (dd/mm/yy)
 */
namespace Multiplayer
{
    /// <summary>
    /// Abstract class to handle game behavior (server and client) with data package
    /// </summary>
    public abstract class DataBehavior
    {
        protected byte[] data;

        public DataBehavior(byte[] data)
        {
            this.data = data;
        }

        /// <summary>
        /// Active the default behavior
        /// </summary>
        protected abstract void Active();
    }
}