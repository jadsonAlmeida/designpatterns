/**
 * MoveForward.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 14/08/17 (dd/mm/yy)
 * Revised on: 15/08/17 (dd/mm/yy)
 */
using System;
using UnityEngine;

namespace Multiplayer
{
    public class MoveForward : DataIDBehavior
    {
		public MoveForward(byte[] data, int id) : base(data, id)
        {
            Active();
        }
		
		/// <summary>
        /// Active the default behavior
        /// </summary>
        protected override void Active()
		{
            //float moveForward = BitConverter.ToSingle(data, 1);
            //Vector3 move = new Vector3(0, 0, moveForward);
		}
    }
}