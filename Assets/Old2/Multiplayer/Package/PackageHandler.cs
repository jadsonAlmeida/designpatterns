/**
 * PackageHandler.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 14/08/17 (dd/mm/yy)
 * Revised on: 04/12/17 (dd/mm/yy)
 */
using UnityEngine;

namespace Multiplayer
{
    public enum DataCode : byte
    {
        None,
        MoveForward,
        MoveBack,
        MoveRight,
        MoveLeft,
        Attack,
        TakeDamage
    }

    /// <summary>
    /// Describes and handle the data sended and received about <see cref="Server"/> and <see cref="Client"/>
    /// </summary>
    public sealed class PackageHandler
    {
        /// <summary>
        /// Instance for singleton
        /// </summary>
        static volatile PackageHandler instance;
        /// <summary>
        /// Simple object to hold the singleton instance process
        /// </summary>
        static object syncRoot = new Object();

        /// <summary>
        /// Create a singleton instance if <see cref="instance"/> is null
        /// </summary>
        public static PackageHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                        if (instance == null)
                            instance = new PackageHandler();
                }
                return instance;
            }
        }

        public void SendData()
        {

        }

        /// <summary>
        /// Handles what data means and pass to <see cref="SendData(byte[])"/>
        /// </summary>
        /// <param name="data"></param>
        public void HandleData(byte[] data)
        {
            DataCode command = (DataCode) data[0];
            switch (command)
            {
                case DataCode.None:
                    break;
                case DataCode.MoveForward:
                    break;
                case DataCode.MoveBack:
                    break;
                case DataCode.MoveRight:
                    break;
                case DataCode.MoveLeft:
                    break;
                case DataCode.Attack:
                    break;
                case DataCode.TakeDamage:
                    break;
                default:
                    break;
            }
        }
    }
}