﻿/**
 * DataIDBehavior.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 25/08/17 (dd/mm/yy)
 * Revised on: 25/08/17 (dd/mm/yy)
 */
namespace Multiplayer
{
    public abstract class DataIDBehavior : DataBehavior
    {
        protected int id;

        public DataIDBehavior(byte[] data, int id) : base(data)
        {
            this.id = id;
        }

        protected void FindTransform()
        {

        }
    }
}