/**
 * Player.cs
 * Created by: Jadson Almeida [jadson.sistemas@gmail.com]
 * Created on: 14/08/17 (dd/mm/yy)
 * Revised on: 14/08/17 (dd/mm/yy)
 */
using UnityEngine;

namespace Multiplayer
{
    /// <summary>
    /// Possible directions that player input can handle
    /// </summary>
    public enum InputPlayer : byte
    {
        None = 0,
        Forward = 1,
        Right = 2,
        Back = 4,
        Left = 8
    }
    
    /// <summary>
    /// The player's attributes and basic behaviors
    /// </summary>
    public class Player : MonoBehaviour
    {
        [SerializeField]
        int classId;
        [SerializeField]
        float moveSpeed;
        [SerializeField]
        float attackSpeed;
        [SerializeField]
        float damage;

        int playerId;
    }
}